## Schulungsmaterialien veröffentlichen

Präsentationsfolien und Schulungsmaterialien der Bibliothek unter offenen Lizenzen zur Nachnutzung bereitstellen.

## Erläuterung des Kriteriums

Schulungsmaterialien werden online und zur Nachnutzung verfügbar gemacht. Gemeint ist didaktisches Material, welches primär zur Schulung und Information von Nutzerinnen und Nutzern vorbereitet wird. Die Materialien müssen unter einer [Open-Definition-konformen Lizenz](http://opendefinition.org/licenses/) stehen.

## Nachweis des Kriteriums

Link(s) zu verfügbaren Materialien und Link zu Blog, Newsticker, Homepage o. ä., über das das Angebot beworben wird