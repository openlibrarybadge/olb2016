# Kriterien für den Open Library Badge 2016

Hier werden die verabschiedeten Kriterien für den Open Library Badge 2016 dokumentiert.

Mehr Infos auf der Webseite <https://badge.openbiblio.eu>

## Kriterien 2016

1. [Open-Digitization-Policy verabschieden](01-open-digitization.md)
2. [Fotos der Bibliothek nachnutzbar machen](02-fotos-der-bibliothek.md)
3. [Schulungsmaterialien veröffentlichen](03-schulungsmaterialien.md)
4. [Open-Access-Potenziale aufdecken](04-open-access-potenziale.md)
5. [Selbst nur Open Access publizieren](05-selbst-open-access.md)
6. [Text Mining ermöglichen](06-text-mining.md)
7. [Open-Access-Semesterapparate entwickeln](07-open-access-semesterapparate.md)
8. [Nutzerinnen einbinden](08-nutzerinnen-einbinden.md)
9. [Open-Source-Software einsetzen](09-open-source.md)
10. [Wikipedia unterstützen](10-wikipedia.md)

