## Wikipedia unterstützen

Wikipedia-Events (z.B. im Rahmen der Open-Access-Week) veranstalten und/oder bibliothekarische Projekte zur Verbesserung der Wikipedia durchführen.

## Erläuterung des Kriteriums

\1. „Wikipedia-Events“: Dies können z.B. Editathons zu bestimmten Themen in Wikipedia, Wikidata, Open Street Map oder anderen freien Wissens-Projekten sein, die von der Bibliothek organisiert werden und/oder in ihren Räumen stattfinden. Nachweis: Das Event sollte als Einladungs-News auf der Website, als Bericht im Blog o.ä. dokumentiert sein.

\2. Anreicherung der Wikipedia durch SpezialistInnen an der Bibliothek: Z.B. Ergänzen fehlender Quellennachweise (Variante: mit Nachweis von Dokumenten aus einer bibliothekseigenen Spezialsammlungen), oder OA-Versionen für Quellen zu recherchieren und verlinken.

## Nachweis des Kriteriums

Link zur Webseite mit Einladung zu einer Veranstaltung, Bericht im Blog o. Ä. Alternativ: Bearbeitungsgeschichte und Wikipedia-Benutzernamen der jeweiligen BibliotheksmitarbeiterInnen.