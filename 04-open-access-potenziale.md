## Open-Access-Potenziale aufdecken

Die Open-Access-Potenziale einzelner WissenschaftlerInnen bei dissem.in recherchieren, diesen zur Kenntnis geben und konkrete Hilfestellung bei der Zweitveröffentlichung im Repositorium der Hochschule anbieten.

## Erläuterung des Kriteriums

Ein konkretes, individuelles Beratungsangebot zu Zweitveröffentlichungsrechten mit persönlichem Kontakt muss mindestens auf der Webseite beworben werden. Entsprechende Tools wie z.B. SherpaRomeo / [dissem.in](http://dissem.in) / ImpactStory sollten dabei zum Einsatz kommen

## Nachweis des Kriteriums

Link zu Beratungsangebot oder Service auf Homepage der Bibliothek. Es sollten bereits erfolgreiche Beratungen statt gefunden haben, nachzuweisen durch entsprechende Listen im eigenen Repositorium.