## Nutzerinnen einbinden

Workshops und/oder Wettbewerbe mit NutzerInnen der Bibliothek zur Verbesserung der Dienstleistungen durchführen.

## Erläuterung des Kriteriums

Ein Workshop oder ein Wettbewerb sollten unter Beteiligung von NutzerInnen durchgeführt und öffentlich (z. B. im Blog) dokumentiert worden sein, möglichst einschließlich konkreter Ergebnisse. Besonders hervorzuheben sind Anregungen oder Vorschläge der NutzerInnen, die von der Bibliothek umgesetzt wurden, auch dies öffentlich dokumentiert.

## Nachweis des Kriteriums

Link zur öffentlichen Dokumentation des Workshops oder Wettbewerbs.