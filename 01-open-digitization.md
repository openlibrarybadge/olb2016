## Open-Digitization-Policy verabschieden

Mit einer Policy eigene Digitalisate und deren Metadaten unter offende Lizenzen stellen.

## Erläuterung des Kriteriums

Eine Open Data Policy für Digitalisate informiert über die Nutzungsbedingungen der Digitalisate und der zugehörigen Metadaten. Digitalisate gemeinfreier Werke werden mithilfe des [CC Public Domain Mark](https://creativecommons.org/publicdomain/mark/1.0/) als gemeinfrei gekennzeichnet. Digitalisate von urheberrechtlich geschützten Werken werden nach Möglichkeit unter einer [Open-Definition-konformen Lizenz](http://opendefinition.org/licenses/) gestellt. Metadaten zu den Digitalisaten werden unter [CC0](https://creativecommons.org/publicdomain/zero/1.0/) lizenziert und sind damit ohne Einschränkungen durch Dritte nutzbar.

## Nachweis des Kriteriums

Link zu Open-Data-Policy für Digitalisate; alternativ: Link zu Blog, Newsticker, Homepage o. ä., über die die offene Lizenzierung der Digitalisate beworben wird oder Link zu API, über die Lizenzinformationen für Digitalisate und Metadaten abgerufen werden kann.