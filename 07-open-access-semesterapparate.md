## Open-Access-Semesterapparate entwickeln

In einem Pilotprojekt mit einem Schwerpunktbereich der eigenen Hochschule gemeinsam relevante Open-Access-Quellen des Fachs kuratieren, im Katalog verzeichnen und als Semesterapparat für die Lehre anbieten.

## Erläuterung des Kriteriums

Sollte auf den zentralen Webseiten der jeweiligen Bibliothek (für die Zielgruppe Studierende) genannt und verlinkt sein. Hervorzuheben sind insbesondere Semesterapparate, die auch ohne Authentifizierung frei einsehbar sind.

## Nachweis des Kriteriums

Beschreibung des Semesterapparates auf der Webseite der Bibliothek