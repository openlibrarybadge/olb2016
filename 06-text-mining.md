## Text Mining ermöglichen

Bei Neuverhandlung und Verlängerung von Lizenzverträgen das Recht auf Text Mining mit verhandeln und die NutzerInnen entsprechend informieren. Gegebenfalls die The Hague Declaration unterzeichnen.

## Erläuterung des Kriteriums

In Europa gibt es derzeit keine einheitliche Schrankenregelung für Text- bzw. Datamining im wissenschaftlichen Kontext. Daher sollten wissenschaftliche Bibliotheken entsprechende Nutzungsrechte für Hochschulangehörige beim Abschluss von Lizenzverträgen vorsorglich explizit mitverhandeln. Dies entspricht auch den Richtlinien, wie sie die DFG bei der Lizenzierung in Fachinformationsdiensten verfolgt (vgl. Musterlizenztext des [FID-Lizenzen](http://www.fid-lizenzen.de/ueber-fid-lizenzen/fid-lizenzen)). Darüber hinaus ist ein politisches Statement, z. B. im Rahmen der The Hague Declaration, empfehlenswert, um den Bedarf einer rechtlichen Regelung zu unterstreichen.

## Nachweis des Kriteriums

Link zu einer Informationsseite der Bibliothek zu Text- und Data Mining mit Angabe zu vorhandenen elektronischen Ressourcen für die entsprechende Nutzungsrechte vorliegen.