## Selbst nur Open Access publizieren

## Erläuterung des Kriteriums

Eine interne Open-Access-Policy für die Bibliothek verabschieden, die über diejenige der Hochschule hinausgeht. Nur noch in Medien publizieren, begutachten, und Herausgeberschaften übernehmen, die denen die Inhalte ohne Zeitverzug Open Access bereitstehen. Die Publikationslisten der Bibliothek mit Direktlinks zu freien Volltexten versehen.

## Nachweis des Kriteriums

Das Kriterium lässt sich durch einen der folgenden Nachweise erfüllen:

a) Verabschiedung einer verpflichtenden Open-Access-Policy für MitarbeiterInnen der Bibliothek, die Autoren-, Gutachter- und Herausgebertätigkeiten betrifft (bitte URL angeben)

ODER

b) Nachweis in Bibliografie oder Jahresbericht der Bibliothek oder Hochschule für die letzten 12 Monate, dass alle Veröffentlichungen der Bibliothek und ihrer MitarbeiterInnen Open Access zur Verfügung stehen mit entsprechenden Links.