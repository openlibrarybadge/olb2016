## Fotos der Bibliothek nachnutzbar machen

Fotos der eigenen Einrichtung unter einer Creative-Commons-Lizenz zur Verfügung stellen.

## Erläuterung des Kriteriums

Fotos der eigenen Einrichtung werden gesammelt und online zur Nachnutzung verfügbar gemacht. Die Fotos müssen unter einer [Open-Definition-konformen Lizenz](http://opendefinition.org/licenses/) stehen.

## Nachweis des Kriteriums

Link zu Bilddatenbank (eigene Datenbank oder Portale wie Wikimedia Commons oder Flickr).