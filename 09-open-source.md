## Open-Source-Software einsetzen

Open-Source-Software bereitstellen, offen mitentwickeln, testen, dokumentieren, übersetzen und/oder einsetzen. Neben oder anstelle von Campuslizenzen auch zu Open-Source-Software beraten.

## Erläuterung des Kriteriums

Die Möglichkeiten Open-Source-Software einzusetzen oder deren Einsatz zu fördern, sind vielfältig. Welche Bibliothek nicht selbst über IT-Ressourcen verfügt, kann bei der Beschaffung von Softwarelösungen Open Source zur Bedingung machen und in der Beratung, z. B. zu Literaturverwaltungsprogrammen, auch stets Open-Source-Alternativen berücksichtigen. Auch systematische Rückmeldungen von Fehlern oder die Schaffung von Schulungsmaterialien stützt die Entwickler von Open-Source-Software. Als zentrale Plattform für Open-Source-Projekte hat sich GitHub etabliert, in der auch kleine Beiträge zu Open-Source-Projekten kenntlich gemacht werden können.

## Nachweis des Kriteriums

Link zum GitHub-Account der Bibliothek (oder deren MitarbeiterInnen) aus dem ein Beitrag zu Open-Source-Projekten in den letzten 12 Monaten deutlich wird. Alternativ Link zu Beratungs- oder Informationsangeboten zu Open Source auf der Webseite der Bibliothek.